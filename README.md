# Wandertag App

A PWA that gives target coordinates and records movement progress.

## Hosted version

Hosted and installable (as a [PWA](https://web.dev/progressive-web-apps/)) under [https://wandertag-app.mathezirkel-augsburg.de/#/](https://wandertag-app.mathezirkel-augsburg.de/#/).

![QR](qr.png)

## Local testing

Run in the base of the project:

```shell
docker compose up
```

See here: [http://localhost:5173/](http://localhost:5173/).

<!--
docker compose run --rm webserver diesel database reset
 -->
