import { createWebHashHistory, createRouter } from "vue-router";

import Index from "./components/Index.vue";
import Tracking from "./components/Tracking.vue";
import View from "./components/View.vue";
import Recordings from "./components/Recordings.vue";
import Manage from "./components/Manage.vue";

const routes = [
    { path: "/", component: Index, name: "index" },
    { path: "/tracking", component: Tracking, name: "tracking" },
    { path: "/view", component: View, name: "view" },
    { path: "/recordings/:slug", component: Recordings, name: "recordings" },
    { path: "/manage", component: Manage, name: "mange" },
];

export default createRouter({
    history: createWebHashHistory(),
    routes,
});
