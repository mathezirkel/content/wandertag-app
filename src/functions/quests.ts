import { Ref, computed, ref, watch } from "vue";
import { availablePaths } from "./paths";
import { errorDialog, questDialog, successDialog } from "./alert";
import { Coordinate, JumpTarget } from "./types";
import toastr from "toastr";

type QuestProgressStorage = {
    [key: string]: {
        [key: number]: {
            reached: boolean;
            success: boolean;
            attempted: boolean;
            optionIndex: number;
        };
    };
};

const questProgressStorageKey = "stored_quest_progress";
const pointSignalDowntimeCooldown = 5 * 1000;

export function setUpQuestComputation(
    selectedPathKey: Ref<string>,
    selectedPointIndex: Ref<number>,
    setPoint: (index: number) => void,
    teleportRendered: Ref<boolean>
) {
    let lastPointUpdateTime = Date.now(); // not a ref, because could cause loops if set in computed

    const questProgressStorage = ref(JSON.parse(localStorage.getItem(questProgressStorageKey) ?? "{}") as QuestProgressStorage);
    watch(
        questProgressStorage,
        () => {
            localStorage.setItem(questProgressStorageKey, JSON.stringify(questProgressStorage.value));
        },
        {
            deep: true,
        }
    );

    function assureQuestStorageEntrySet() {
        if (selectedPathKey.value != "") {
            if (
                !questProgressStorage.value[selectedPathKey.value] ||
                questProgressStorage.value[selectedPathKey.value] == undefined
            ) {
                questProgressStorage.value[selectedPathKey.value] = {};
            }

            if (
                !questProgressStorage.value[selectedPathKey.value][selectedPointIndex.value] ||
                questProgressStorage.value[selectedPathKey.value][selectedPointIndex.value] == undefined
            ) {
                questProgressStorage.value[selectedPathKey.value][selectedPointIndex.value] = {
                    attempted: false,
                    reached: false,
                    success: false,
                    optionIndex: 0,
                };
            }

            // api changed, may need to be added to array
            if (questProgressStorage.value[selectedPathKey.value][selectedPointIndex.value].optionIndex == undefined) {
                questProgressStorage.value[selectedPathKey.value][selectedPointIndex.value].optionIndex = 0;
            }
        }
    }

    function setOptionIndexOnCurrentQuest(index: number) {
        if (selectedPathKey.value != "") {
            assureQuestStorageEntrySet();

            questProgressStorage.value[selectedPathKey.value][selectedPointIndex.value].optionIndex = index;
        }
    }

    function setReachedOnCurrentQuest() {
        if (selectedPathKey.value != "") {
            assureQuestStorageEntrySet();

            questProgressStorage.value[selectedPathKey.value][selectedPointIndex.value].reached = true;
        }
    }

    function setStatusOnCurrentQuest(success: boolean) {
        if (selectedPathKey.value != "") {
            assureQuestStorageEntrySet();

            questProgressStorage.value[selectedPathKey.value][selectedPointIndex.value].attempted = true;
            questProgressStorage.value[selectedPathKey.value][selectedPointIndex.value].success = success;
        }
    }

    function resetQuestProgress() {
        if (selectedPath.value) {
            questProgressStorage.value[selectedPath.value.name] = {};
            setPoint(0);
        }
    }

    const availablePathNames = computed(() => {
        return Object.keys(availablePaths);
    });

    const selectedPath = computed(() => {
        if (Object.keys(availablePaths).includes(selectedPathKey.value)) {
            return availablePaths[selectedPathKey.value];
        } else {
            return null;
        }
    });

    const selectedPathId = computed(() => {
        if (selectedPath.value != null) {
            return selectedPath.value.index;
        } else {
            return 0;
        }
    });

    const numberOfPointsInSelectedPath = computed(() => {
        if (selectedPath.value == null) {
            return 0;
        } else {
            return selectedPath.value.numberOfPoints;
        }
    });

    const coordinatesInSelectedPath = computed(() => {
        if (selectedPath.value == null) {
            return [];
        } else {
            return selectedPath.value.points;
        }
    });

    const targetsInSelectedPath = computed(() => {
        if (selectedPath.value == null) {
            return [];
        } else {
            return selectedPath.value.jumpToIndexTarget;
        }
    });

    const targetOptionsOfSelectedPoint = computed((): JumpTarget[] => {
        const target = targetsInSelectedPath.value[selectedPointIndex.value] ?? 0;

        if (!Array.isArray(target)) {
            // single number
            return [
                {
                    index: target,
                    optionText: "AUTOMATICALLY INSERTED; SHOULD NEVER BE DISPLAYED",
                },
            ] as JumpTarget[];
        }

        return target;
    });

    const targetOptionIndexOfSelectedPoint = computed({
        get: () => {
            return (selectedPathPointQuestProgressStorage.value?.optionIndex ?? 0) as number;
        },
        set: (value: any): void => {
            setOptionIndexOnCurrentQuest(parseInt(value));
        },
    });

    const nextIndex = computed((): number => {
        const optionIndex = targetOptionIndexOfSelectedPoint.value ?? 0;
        const targetOptions = targetOptionsOfSelectedPoint.value ?? [];

        const target = targetOptions[optionIndex];
        if (target == undefined) {
            return 0;
        }
        return target.index;
    });

    const pointNamesInSelectedPath = computed(() => {
        if (selectedPath.value == null) {
            return [];
        } else {
            return selectedPath.value.pointNames;
        }
    });

    const questsInSelectedPath = computed(() => {
        if (selectedPath.value == null) {
            return [];
        } else {
            return selectedPath.value.quests;
        }
    });

    const pointsDropdownValues = computed(() => {
        let res = [] as {
            value: number;
            title: string;
        }[];
        if (selectedPath.value == null) {
            return res;
        } else {
            for (let i = 0; i < numberOfPointsInSelectedPath.value; i++) {
                res[i] = {
                    title: pointNamesInSelectedPath.value[i],
                    value: i,
                };
            }
            return res;
        }
    });

    const selectedPoint = computed((): Coordinate => {
        lastPointUpdateTime = Date.now(); // cache when the last time a point-index was raised
        console.log("New Point selected timout started");
        setTimeout(() => {
            console.log("Point is now hot");
        }, pointSignalDowntimeCooldown);

        if (selectedPath.value == null) {
            return [0, 0];
        } else {
            return coordinatesInSelectedPath.value[selectedPointIndex.value] ?? [0, 0];
        }
    });

    const selectedTargetName = computed((): string | null => {
        if (selectedPath.value == null) {
            return null;
        } else {
            return pointNamesInSelectedPath.value[selectedPointIndex.value] ?? null;
        }
    });

    const selectedQuest = computed((): string | null => {
        if (selectedPath.value == null) {
            return null;
        } else {
            return questsInSelectedPath.value[selectedPointIndex.value] ?? null;
        }
    });

    const selectedPathPointQuestProgressStorage = computed(() => {
        if (selectedPath.value == null) {
            return null;
        } else {
            const selQuestProgress = questProgressStorage.value[selectedPath.value.name];
            if (selQuestProgress) {
                const pointStorage = selQuestProgress[selectedPointIndex.value];
                if (pointStorage) {
                    return pointStorage;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    });

    const selectedPathPointReached = computed((): boolean => {
        if (selectedPathPointQuestProgressStorage.value == null) {
            return false;
        } else {
            return selectedPathPointQuestProgressStorage.value.reached;
        }
    });

    const selectedQuestAttempted = computed((): boolean => {
        if (selectedPathPointQuestProgressStorage.value == null) {
            return false;
        } else {
            return selectedPathPointQuestProgressStorage.value.attempted;
        }
    });

    const selectedQuestSolved = computed((): boolean => {
        if (selectedPathPointQuestProgressStorage.value == null) {
            return false;
        } else {
            return selectedPathPointQuestProgressStorage.value.success;
        }
    });

    const selectedQuestAvailable = computed((): boolean => {
        return selectedQuest.value != null || targetOptionsOfSelectedPoint.value.length > 1;
    });

    const pointMode = computed((): "searching" | "questing" | "finished" => {
        if (selectedQuestAttempted.value || (!selectedQuestAvailable.value && selectedPathPointReached.value)) {
            return "finished";
        }
        if (selectedPathPointReached.value) {
            return "questing";
        }
        return "searching";
    });

    watch(pointMode, (now, prev) => {
        if (prev == "searching" && now == "questing") {
            toastr.info("Angekommen. Löse jetzt die Quest!");
        }
    });

    const selectedQuestReady = computed((): boolean => {
        return pointMode.value == "questing";
    });

    // point index can only be increased each 10 seconds to debounce and avoid zipping all the way to the latest point
    function signalPointReached() {
        toastr.success("Punkt erreicht");

        if (lastPointUpdateTime < Date.now() - pointSignalDowntimeCooldown) {
            setReachedOnCurrentQuest();
            if (selectedQuestAvailable.value) {
                // here is a quest to do
            } else {
                // no quest to do
                setTimeout(() => {
                    selectNextPoint();
                }, 1500);
            }
        }
    }

    function selectNextPoint() {
        if (numberOfPointsInSelectedPath.value > nextIndex.value) {
            toastr.success("Neuer Punkt ausgewählt");

            setPoint(nextIndex.value);
        } else {
            toastr.success("Zielpunkt erreicht!!");
        }
    }

    function handleQuestButton() {
        if (selectedQuestAvailable.value) {
            // button shown

            if (pointMode.value == "questing" || pointMode.value == "finished") {
                // button enabled and not done, or button disabled, because done already. In the latter case stil make the button work

                const quest = selectedQuest.value;
                if (quest != null || targetOptionsOfSelectedPoint.value.length > 1) {
                    // quest check for ts, should always be fullfilled if we are here

                    questDialog(
                        "Quest: " + selectedTargetName.value,
                        quest ?? "",
                        () => {
                            successDialog("Quest erfüllt!", "Fahre fort zum nächsten Punkt", () => {
                                setStatusOnCurrentQuest(true);

                                setTimeout(() => {
                                    selectNextPoint();
                                }, 1000);
                            });
                        },
                        () => {
                            errorDialog("Quest fehlgeschlagen", "Begib dich trotzdem zum nächsten Punkt", () => {
                                setStatusOnCurrentQuest(false);

                                setTimeout(() => {
                                    selectNextPoint();
                                }, 1500);
                            });
                        },
                        teleportRendered
                    );
                }
            }
        } else {
            // direct continuation
            if (pointMode.value == "finished") {
                selectNextPoint();
            }
        }
    }

    return {
        resetQuestProgress,
        availablePathNames,
        selectedPoint,
        selectedPath,
        selectedPathId,
        selectedTargetName,
        pointsDropdownValues,
        selectedQuestSolved,
        selectedQuestAttempted,
        selectedQuestAvailable,
        selectedQuestReady,
        signalPointReached,
        handleQuestButton,
        pointMode,
        targetOptionsOfSelectedPoint,
        targetOptionIndexOfSelectedPoint,
    };
}
