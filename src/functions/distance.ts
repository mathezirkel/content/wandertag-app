// # https://www.movable-type.co.uk/scripts/latlong.html
export function calculateDistanceInMeters(lat1: number, lon1: number, lat2: number, lon2: number) {
    const R = 6371e3; // metres
    const phi1 = (lat1 * Math.PI) / 180; // φ, λ in radians
    const phi2 = (lat2 * Math.PI) / 180;
    const delphi = ((lat2 - lat1) * Math.PI) / 180;
    const dellam = ((lon2 - lon1) * Math.PI) / 180;

    const a =
        Math.sin(delphi / 2) * Math.sin(delphi / 2) +
        Math.cos(phi1) * Math.cos(phi2) * Math.sin(dellam / 2) * Math.sin(dellam / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const d = R * c; // in metres

    return d;
}
