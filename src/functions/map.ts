import { Map, View, Feature } from "ol";
import { Tile as TileLayer, Vector as VectorLayer } from "ol/layer";
import { OSM, Vector as VectorSource } from "ol/source";
import { Polyline } from "ol/format";
import { Style, Stroke, Icon } from "ol/style";
import polyline from "@mapbox/polyline";
import { convertToOSMCoordinates } from "../functions/coordinates";
import { Line, LineDataExport, LineDataExportElement, PhotoMeta } from "./types";
import { Point } from "ol/geom";
import { Select } from "ol/interaction";
import { click } from "ol/events/condition";

let map = null as null | Map;

export function initMap(initalZoom: number = 14, initialLat: number = 48.45357177939096, initialLon: number = 10.60930317960719) {
    // init map
    map = new Map({
        layers: [
            new TileLayer({
                source: new OSM(),
            }),
        ],
        target: "map",
        view: new View({
            zoom: initalZoom,
            center: convertToOSMCoordinates(initialLat, initialLon),
        }),
    });
}

export function addLineToMap(
    line: Line,
    width: number = 5,
    color: [number, number, number, number] = [237, 212, 0, 0.8],
    dashed: boolean = false
) {
    const route = new Polyline({
        factor: 1e5,
    }).readGeometry(polyline.encode(line), {
        dataProjection: "EPSG:4326",
        featureProjection: "EPSG:3857",
    });

    const routeFeature = new Feature({
        type: "route",
        geometry: route,
    });

    const styles = {
        route: new Style({
            stroke: new Stroke({
                width: width,
                color: color,
                lineDash: dashed ? [30, 5, 15, 5] : undefined,
            }),
        }),
    };

    const vectorLayer = new VectorLayer({
        source: new VectorSource({
            features: [routeFeature],
        }),
        style: function (feature) {
            return styles[feature.get("type") as "route"];
        },
    });

    if (map) {
        map.addLayer(vectorLayer);
    }
}

export function addPhotoMarkerToMap(photo: PhotoMeta, callback: (id: number) => void) {
    const photoId = photo[0];
    const lat = photo[2];
    const lon = photo[3];

    const photoFeature = new Feature({
        geometry: new Point([lon, lat]).transform("EPSG:4326", "EPSG:3857"),
        photoId: photoId,
    });

    const photoStyle = new Style({
        image: new Icon({
            anchor: [0.5, 1],
            src: "/pin_icon.png", // Path to your photo icon
            scale: 0.08, // Adjust scale as needed
        }),
    });

    photoFeature.setStyle(photoStyle);

    const vectorLayer = new VectorLayer({
        source: new VectorSource({
            features: [photoFeature],
        }),
    });

    if (map) {
        map.addLayer(vectorLayer);

        const selectClick = new Select({
            condition: click,
            layers: [vectorLayer],
        });

        selectClick.on("select", function (e) {
            if (e.selected.length > 0) {
                const feature = e.selected[0];
                const photoId = feature.get("photoId");

                console.log(`Photo marker clicked: ${photoId}`);
                callback(photoId);
            }
        });

        map.addInteraction(selectClick);
    }
}

export function clearLinesFromMap() {
    if (map) {
        const layers = map.getAllLayers() ?? [];

        layers.forEach((layer) => {
            if (layer instanceof VectorLayer) {
                map?.removeLayer(layer);
            }
        });
    }
}

export function lineDataExportToLines(ldexport: LineDataExport): Line[] {
    // LineDataExport [id, timeStamp, lat, lon]

    const groupedById: Record<number, LineDataExportElement[]> = {};

    ldexport.forEach((entry) => {
        const [id] = entry;
        if (!groupedById[id]) {
            groupedById[id] = [];
        }
        groupedById[id].push(entry);
    });

    const sortedGroups: LineDataExportElement[][] = Object.values(groupedById).map((group) => {
        return group.sort((a, b) => a[1] - b[1]);
    });

    const splitAndSortedGroups: LineDataExportElement[][][] = sortedGroups.map((group) => {
        const splitGroup: LineDataExportElement[][] = [];
        let currentSubGroup: LineDataExportElement[] = [];

        for (let i = 0; i < group.length; i++) {
            if (currentSubGroup.length === 0) {
                currentSubGroup.push(group[i]);
            } else {
                const lastEntry = currentSubGroup[currentSubGroup.length - 1];
                if (group[i][1] - lastEntry[1] <= 1000 * 60 * 10) {
                    currentSubGroup.push(group[i]);
                } else {
                    splitGroup.push(currentSubGroup);
                    currentSubGroup = [group[i]];
                }
            }
        }
        if (currentSubGroup.length > 0) {
            splitGroup.push(currentSubGroup);
        }

        return splitGroup;
    });

    const result: Line[] = splitAndSortedGroups.flat().map((group) => {
        return group.map((entry) => [entry[2], entry[3]]);
    });

    return result;
}
