import { fromLonLat } from "ol/proj";

export function convertToOSMCoordinates(lat: number, lon: number) {
    return fromLonLat([lon, lat]);
}
