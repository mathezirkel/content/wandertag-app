import SeedRandom from "seed-random";
import paths from "./../paths.json";
import { Coordinate, JumpTarget, Path } from "./types";
import { error } from "toastr";

const importedPaths = paths as unknown as PathFileImport;

type PathDict = {
    [key: string]: Path;
};
type PathFileImport = {
    [key: string]: {
        point: Coordinate;
        quest?: string;
        name?: string;
        label?: string;
        jump?: string;
        options?: {
            text: string;
            jump: string;
        }[];
    }[];
};

function computeAvailablePaths(): PathDict {
    let res = {} as PathDict;

    for (const key in importedPaths) {
        // could support more advanced features in the future
        const path = importedPaths[key];

        // check jump integrety
        let jumpTargets = {} as {
            [key: string]: number;
        };
        path.forEach((pointWDefinition, i) => {
            if (pointWDefinition.label != undefined) {
                jumpTargets[pointWDefinition.label] = i;
            }
        });
        path.forEach((pointWDefinition) => {
            if (pointWDefinition.jump != undefined && pointWDefinition.options != undefined) {
                error("DEFINITION ERROR; CAN NOT HAVE JUMP AND OPTIONS");
            }
            if (pointWDefinition.jump != undefined) {
                if (!Object.keys(jumpTargets).includes(pointWDefinition.jump)) {
                    error(`DEFINITION ERROR; JUMP TARGET NOT DEFINED ${pointWDefinition.jump}`);
                }
            }
            if (pointWDefinition.options != undefined) {
                if (pointWDefinition.options.length <= 1) {
                    error(`DEFINITION ERROR; Must have at least 2 options defined where the key is set`);
                }
                pointWDefinition.options.forEach((opt) => {
                    if (!Object.keys(jumpTargets).includes(opt.jump)) {
                        error(`DEFINITION ERROR; JUMP TARGET NOT DEFINED ${opt.jump}`);
                    }
                });
            }
        });
        // checked jump integrety

        const numberOfPoints = path.length;

        res[key] = {
            points: path.map((pointWDefinition) => {
                return pointWDefinition.point;
            }),
            pointNames: path.map((pointWDefinition, i) => {
                if (pointWDefinition.name) {
                    return pointWDefinition.name;
                } else {
                    return "Punkt Nr. " + (i + 1);
                }
            }),
            quests: path.map((pointWDefinition) => {
                if (pointWDefinition.quest) {
                    return pointWDefinition.quest;
                } else {
                    return null;
                }
            }),
            jumpToIndexTarget: path.map((pointWDefinition, i): number | JumpTarget[] => {
                if (pointWDefinition.jump != undefined) {
                    return jumpTargets[pointWDefinition.jump];
                }
                if (pointWDefinition.options != undefined) {
                    return pointWDefinition.options.map((option): JumpTarget => {
                        return {
                            index: jumpTargets[option.jump],
                            optionText: option.text,
                        };
                    });
                }
                return i + 1; // default advance to next target
            }),
            numberOfPoints,
            name: key,
            index: generateRandomId(key),
        };
    }

    return res;
}

function generateRandomId(seed: string): number {
    const rng = SeedRandom(seed);
    return Math.floor(rng() * 10000000);
}

export const availablePaths: PathDict = computeAvailablePaths();

function getWayPoints(): { [key: number]: Coordinate[][] } {
    const res: { [key: number]: Coordinate[][] } = {};
    Object.keys(availablePaths).forEach((key) => {
        const path = availablePaths[key];

        function recursiveCoordinates(index: number): Coordinate[][] {
            const ownPoint = path.points[index];
            const target = path.jumpToIndexTarget[index];

            let targetIndices: number[];
            if (Array.isArray(target)) {
                targetIndices = target.map((t) => {
                    return t.index;
                });
            } else {
                targetIndices = [target];
            }

            const res: Coordinate[][] = [];

            targetIndices.forEach((targetIndex) => {
                if (targetIndex == index) {
                    res.push([]);
                } else {
                    recursiveCoordinates(targetIndex).forEach((rec) => {
                        res.push([
                            ownPoint,
                            ...rec.filter((coord) => {
                                return coord != undefined;
                            }),
                        ]);
                    });
                }
            });

            return res;
        }

        res[path.index] = recursiveCoordinates(0);
    });

    return res;
}

export const wayPoints = getWayPoints();
