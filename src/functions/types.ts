export type Coordinate = [number, number];
export type JumpTarget = {
    index: number;
    optionText: string;
};
export type Path = {
    points: Coordinate[];
    pointNames: string[];
    quests: (string | null)[];
    jumpToIndexTarget: (number | JumpTarget[])[];
    numberOfPoints: number;
    name: string;
    index: number;
};
export type PhotoMeta = [
    number, // id
    number, // timestamp
    number, // lat
    number, // lon
    number // quest id
];
export type LineDataExport = LineDataExportElement[];
export type PhotoMetaExport = PhotoMeta[];
export type LineDataExportElement = [
    number, // random id
    number, // timestamp
    number, // lat
    number, // lon
    number // quest id
];
export type Line = Coordinate[];
