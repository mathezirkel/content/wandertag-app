import Swal from "sweetalert2";
import { Ref, ref, nextTick } from "vue";

// https://sweetalert2.github.io/

export interface OptionsArr {
    value: number;
    text: string;
}

export function questDialog(
    title: string,
    text: string,
    successCallback: () => void = () => {},
    errorCallback: () => void = () => {},
    teleportRendered: Ref<boolean>
) {
    generalDialog(
        title,
        text,
        "question",
        true,
        true,
        true,
        false,
        true,
        () => {},
        successCallback,
        errorCallback,
        teleportRendered
    );
}

export function successDialog(title: string, text: string, callback: () => void = () => {}) {
    generalDialog(title, text, "success", false, true, false, true, false, callback);
}

export function errorDialog(title: string, text: string, callback: () => void = () => {}) {
    generalDialog(title, text, "error", false, true, false, true, false, callback);
}

function generalDialog(
    title: string,
    text: string,
    icon: string,
    showCancelButton: boolean,
    showConfirmButton: boolean,
    showDenyButton: boolean,
    allowOutsideClick: boolean,
    bigExpansion: boolean = false,
    alwaysCallback: () => void = () => {},
    successCallback: () => void = () => {},
    failureCallback: () => void = () => {},
    teleportRendered: Ref<boolean> = ref(false)
) {
    teleportRendered.value = false;

    nextTick(() => {
        let optionHTML = `<div id="teleport-target" style="width: 90%; margin: auto; display: inherit;"></div>`;

        Swal.fire({
            title,
            html: '<div style="white-space: pre-line; text-align: left;">' + text + "</div>" + optionHTML,
            icon: icon as any,
            showCancelButton,
            showConfirmButton,
            showDenyButton,
            confirmButtonText: "Fertig",
            cancelButtonText: "Abbrechen",
            denyButtonText: "Fehlschlag",
            allowOutsideClick,
            customClass: bigExpansion ? "bigexpansionclasss" : "",
            didOpen: () => {
                teleportRendered.value = true;
            },
        }).then((result) => {
            if (result.isConfirmed) {
                successCallback();
            } else if (result.isDenied) {
                failureCallback();
            }
            alwaysCallback();
        });
    });
}
