import { calculateDistanceInMeters } from "./distance";
import { Coordinate } from "./types";

const safeZoneCoordinates: Coordinate[] = [[48.450877808824906, 10.573632543599086]];
const safeZoneCoordinateRadius = 12000;

export function closeEnoughToAnySafePoint(lat: number, lon: number): boolean {
    let closeEnoughToAnySafePoint = false;
    safeZoneCoordinates.forEach((coord) => {
        const distanceToLastSafeCoordinate = calculateDistanceInMeters(lat, lon, coord[0], coord[1]);

        if (distanceToLastSafeCoordinate < safeZoneCoordinateRadius) {
            closeEnoughToAnySafePoint = true;
        }
    });

    return closeEnoughToAnySafePoint;
}

export function jungEnough(timestamp: number): boolean {
    const currentTime = Date.now();

    // not older than 30 hours
    return !(timestamp + 1000 * 60 * 60 * 30 < currentTime);
}
