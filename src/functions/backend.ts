import { LineDataExport, PhotoMetaExport } from "./types";
import toastr from "toastr";

export function getBackendURL(path: string) {
    return `${(process as any).env.BACKEND_SERVER_URL}${path}`;
}

export async function getPointsAndPhotos(password: string, from: number, to: number): Promise<[LineDataExport, PhotoMetaExport]> {
    let points = [] as LineDataExport;
    let photos = [] as PhotoMetaExport;

    if (password != "") {
        points = await fetch(getBackendURL("request-data?secret=" + encodeURIComponent(password) + "&from=" + from + "&to=" + to))
            .then(function (response) {
                if (response.ok) {
                    console.log("Datapoints could be loaded");
                    toastr.success("Neue Daten geladen");

                    return response.json();
                } else {
                    toastr.error("Falsches Passwort oder Fehler");

                    return getEmptyArrayPromise();
                }
            })
            .catch((error) => {
                console.error("Could not load datapoint-JSON", error);
                toastr.error("Unbekannter Fehler");
            });
        photos = await fetch(
            getBackendURL("request-photo-meta?secret=" + encodeURIComponent(password) + "&from=" + from + "&to=" + to)
        )
            .then(function (response) {
                if (response.ok) {
                    console.log("Photo Meta could be loaded");
                    toastr.success("Neue Photos geladen");

                    return response.json();
                } else {
                    toastr.error("Falsches Passwort oder Fehler");
                    return getEmptyArrayPromise();
                }
            })
            .catch((error) => {
                console.error("Could not load photo-meta-JSON", error);
                toastr.error("Unbekannter Fehler");
            });
    }

    return [points, photos];
}

function getEmptyArrayPromise() {
    return new Promise((resolve, _) => {
        resolve([1]);
    });
}
