const indexDbVersion = 1;
const indexDbName = "ImageDatabase";
const indexDbObjectStoreName = "ImageObjectStore";

export function writeToIndexedDB(key: string, value: any): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        const request = indexedDB.open(indexDbName, indexDbVersion);

        request.onupgradeneeded = function () {
            const db = request.result;
            db.createObjectStore(indexDbObjectStoreName, { keyPath: "id" });
        };

        request.onsuccess = function () {
            const db = request.result;
            const transaction = db.transaction([indexDbObjectStoreName], "readwrite");
            const objectStore = transaction.objectStore(indexDbObjectStoreName);

            const putRequest = objectStore.put({ id: key, value: value });
            putRequest.onsuccess = function () {
                resolve();
            };
            putRequest.onerror = function () {
                reject("Error storing value in IndexedDB");
            };
        };

        request.onerror = function () {
            reject("Error opening IndexedDB");
        };
    });
}

export function removeFromIndexedDB(key: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        const request = indexedDB.open(indexDbName, indexDbVersion);

        request.onupgradeneeded = function () {
            const db = request.result;
            db.createObjectStore(indexDbObjectStoreName, { keyPath: "id" });
        };

        request.onsuccess = function () {
            const db = request.result;
            const transaction = db.transaction([indexDbObjectStoreName], "readwrite");
            const objectStore = transaction.objectStore(indexDbObjectStoreName);

            const putRequest = objectStore.delete(key);
            putRequest.onsuccess = function () {
                resolve();
            };
            putRequest.onerror = function () {
                reject("Error deleting value from IndexedDB");
            };
        };

        request.onerror = function () {
            reject("Error opening IndexedDB");
        };
    });
}

export function readFromIndexedDB(key: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
        const request = indexedDB.open(indexDbName, indexDbVersion);

        request.onsuccess = function () {
            const db = request.result;
            const transaction = db.transaction([indexDbObjectStoreName], "readonly");
            const objectStore = transaction.objectStore(indexDbObjectStoreName);

            const getRequest = objectStore.get(key);
            getRequest.onsuccess = function (event) {
                const data = (event.target as any).result;
                if (data) {
                    resolve(data.value);
                } else {
                    reject("Key not found in IndexedDB");
                }
            };
        };

        request.onerror = function () {
            reject("Error opening IndexedDB");
        };
    });
}

export function allKeysFromIndexedDB(): Promise<string[]> {
    return new Promise<string[]>((resolve, reject) => {
        const request = indexedDB.open(indexDbName, indexDbVersion);

        request.onsuccess = function () {
            const db = request.result;
            const transaction = db.transaction([indexDbObjectStoreName], "readonly");
            const objectStore = transaction.objectStore(indexDbObjectStoreName);

            const getRequest = objectStore.getAllKeys();
            getRequest.onsuccess = function (event) {
                const data = (event.target as any).result;
                if (data) {
                    resolve(data);
                } else {
                    reject("All Keys method of IndexedDB returned error");
                }
            };
        };

        request.onerror = function () {
            reject("Error opening IndexedDB");
        };
    });
}
