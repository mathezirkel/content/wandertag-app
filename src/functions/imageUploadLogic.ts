import { Coordinate } from "./types";
import toastr from "toastr";
import { readFromIndexedDB, writeToIndexedDB, allKeysFromIndexedDB, removeFromIndexedDB } from "./database";
import { computed, ref, toRaw } from "vue";
import { v4 as uuidv4 } from "uuid";
import { getBackendURL } from "./backend";
import { jungEnough } from "./safezone";

interface ImageStorage {
    imageData: string;
    coordinates: Coordinate;
    timestamp: number;
    questId: number;
}

const keysToUpload = ref([] as string[]);
const firstKey = computed(() => {
    if (numberOfPhotosInCache.value == 0) {
        return null;
    }
    return keysToUpload.value[0];
});

async function updateKeysRef() {
    await removeFromIndexedDB("placeholder"); // assure the db gets initialized

    keysToUpload.value = (await allKeysFromIndexedDB()).filter((key) => {
        return key.startsWith("upload__");
    });
}

export const numberOfPhotosInCache = computed(() => {
    return keysToUpload.value.length;
});

export async function handlePhotoUpload(questId: number, coordinates: Coordinate, base64Photo: string) {
    const usableQuestId = toRaw(questId);
    const usableCoordinates = toRaw(coordinates);
    const usablePhoto = toRaw(base64Photo);
    const imageData: ImageStorage = {
        coordinates: usableCoordinates,
        imageData: usablePhoto,
        timestamp: Date.now(),
        questId: usableQuestId,
    };

    await writeToIndexedDB("upload__" + uuidv4(), imageData);
    await updateKeysRef();

    toastr.success("Foto für den upload vorbereitet");
}

updateKeysRef();

setInterval(async () => {
    // try uploading a photo
    await updateKeysRef();

    if (numberOfPhotosInCache.value > 0) {
        const keyToTreat = firstKey.value;
        if (keyToTreat) {
            console.log("try uploading entry", keyToTreat);

            await readFromIndexedDB(keyToTreat).then(async (data: ImageStorage) => {
                console.log("photo to upload fetched from indexed db", keyToTreat);

                let time = data.timestamp;
                if (!jungEnough(time)) {
                    // older than allowed
                    console.error("Photo in cache longer than 30 hours, clearing it");
                    await removeFromIndexedDB(keyToTreat);
                } else {
                    fetch(getBackendURL("upload-photo"), {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify({
                            quest_id: data.questId,
                            lat: data.coordinates[0],
                            lon: data.coordinates[1],
                            time: data.timestamp,
                            image: data.imageData,
                        }),
                    }).then(async (dataResponse: Response) => {
                        let data: { success: boolean } = await dataResponse.json();

                        const worked = data.success && data.success != undefined && data.success != null;
                        if (worked) {
                            // clear from cache
                            toastr.success("Foto hochgeladen");

                            await removeFromIndexedDB(keyToTreat);
                        }
                        updateKeysRef();
                    });
                }
            });
        }
    }
}, 20 * 1000);

// TODO could cache in database - however would be needed to cache bust then, to not store data locally
export async function getImageData(password: string, id: number): Promise<string> {
    const res = await fetch(getBackendURL("request-photo?secret=" + encodeURIComponent(password) + "&id=" + String(id))).catch(
        (error) => {
            console.error("Could not load photo-meta-JSON", error);
            toastr.error("Unbekannter Fehler");
        }
    );

    if (!res) {
        return "";
    }

    if (res.ok) {
        const data = await res.json();

        return data.image;
    } else {
        toastr.error("Falsches Passwort oder Fehler");
    }

    return ""; // should not happen
}
