use crate::models::ViewControlStruct;
use crate::{environment::check_secret, models::InsertViewControlStruct};
use actix_web::{web, HttpResponse, Responder};
use serde::Deserialize;
use std::collections::HashMap;

pub async fn get_all_control_structs_route(
    query: web::Query<HashMap<String, String>>,
) -> impl Responder {
    debug!("Requested control structs");
    let secret = query.get("secret").map_or("", String::as_str);
    let access: bool = check_secret(secret);

    if access {
        debug!("Access granted, sending data");

        let res: Vec<ViewControlStruct> = ViewControlStruct::find_all().unwrap(); // cannot be bothered to do proper handling here

        HttpResponse::Ok().json(res)
    } else {
        debug!("Access not granted");
        HttpResponse::Unauthorized().finish()
    }
}

pub async fn create_control_structs_route(
    query: web::Query<HashMap<String, String>>,
) -> impl Responder {
    debug!("Requested control struct creation");
    let secret = query.get("secret").map_or("", String::as_str);
    let access: bool = check_secret(secret);

    if access {
        debug!("Access granted, creating...");

        let _ = ViewControlStruct::create(InsertViewControlStruct {
            slug: String::from("replaceme"),
            name: String::from("replaceme"),
            password: String::from("replaceme"),
            quest_selections: Vec::new(),
        });

        let res: Vec<ViewControlStruct> = ViewControlStruct::find_all().unwrap(); // cannot be bothered to do proper handling here

        HttpResponse::Ok().json(res)
    } else {
        debug!("Access not granted");
        HttpResponse::Unauthorized().finish()
    }
}

#[derive(Deserialize)]
pub struct UpdateControlStructsPayload {
    pub id: i32,
    pub data: InsertViewControlStruct,
}

pub async fn update_control_structs_route(
    query: web::Query<HashMap<String, String>>,
    data: web::Json<UpdateControlStructsPayload>,
) -> impl Responder {
    debug!("Requested control struct update");
    let secret = query.get("secret").map_or("", String::as_str);
    let access: bool = check_secret(secret);

    if access {
        debug!("Access granted, updating...");

        let upload = data.into_inner();

        let _ = ViewControlStruct::update(upload.id, upload.data);

        let res: Vec<ViewControlStruct> = ViewControlStruct::find_all().unwrap(); // cannot be bothered to do proper handling here

        HttpResponse::Ok().json(res)
    } else {
        debug!("Access not granted");
        HttpResponse::Unauthorized().finish()
    }
}

#[derive(Deserialize)]
pub struct DeleteControlStructsPayload {
    pub id: i32,
}

pub async fn delete_control_structs_route(
    query: web::Query<HashMap<String, String>>,
    data: web::Json<DeleteControlStructsPayload>,
) -> impl Responder {
    debug!("Requested control struct update");
    let secret = query.get("secret").map_or("", String::as_str);
    let access: bool = check_secret(secret);

    if access {
        debug!("Access granted, updating...");

        let upload = data.into_inner();

        let _ = ViewControlStruct::delete(upload.id);

        let res: Vec<ViewControlStruct> = ViewControlStruct::find_all().unwrap(); // cannot be bothered to do proper handling here

        HttpResponse::Ok().json(res)
    } else {
        debug!("Access not granted");
        HttpResponse::Unauthorized().finish()
    }
}
