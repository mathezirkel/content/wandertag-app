use crate::environment::{check_secret, upload_active};
use crate::models::{Datapoint, InsertDatapoint, InsertPhoto, Photo, ViewControlStruct};
use actix_web::http::header::ContentType;
use actix_web::{web, HttpResponse, Responder};
use serde::Serialize;
use std::collections::HashMap;
use std::fmt::Write;

#[derive(Serialize)]
struct RedeemResponse {
    success: bool,
}

pub async fn upload_route(data: web::Json<HashMap<String, InsertDatapoint>>) -> impl Responder {
    if !upload_active() {
        debug!("Upload not activated");
        return HttpResponse::Forbidden()
            .content_type(ContentType::plaintext())
            .body("Upload Deactivated");
    }

    debug!("Started Upload");

    for (_, value) in data.0 {
        Datapoint::create(value).unwrap();
    }

    let response = RedeemResponse { success: true };
    debug!("Successfully uploaded data to server");
    HttpResponse::Ok().json(response)
}

pub async fn upload_photo_route(data: web::Json<InsertPhoto>) -> impl Responder {
    if !upload_active() {
        debug!("Upload not activated");
        return HttpResponse::Forbidden()
            .content_type(ContentType::plaintext())
            .body("Upload Deactivated");
    }

    debug!("Started Upload");

    let photo = data.into_inner();

    Photo::create(photo).unwrap();

    let response = RedeemResponse { success: true };
    debug!("Successfully uploaded photo to server");
    HttpResponse::Ok().json(response)
}

pub async fn request_route(query: web::Query<HashMap<String, String>>) -> impl Responder {
    debug!("Requested points");
    let secret = query.get("secret").map_or("", String::as_str);
    let access: bool = check_secret(secret);

    let from = query
        .get("from")
        .and_then(|f| f.parse::<i64>().ok())
        .unwrap_or(1264979220000); // 1264979220000 = 01.01.2010
    let to = query
        .get("to")
        .and_then(|t| t.parse::<i64>().ok())
        .unwrap_or(2527283220000); // 2527283220000 = 01.01.2050

    if access {
        debug!("Access granted, sending data");

        HttpResponse::Ok()
            .content_type(ContentType::json())
            .body(points_response_aggregator(from, to, -1))
    } else {
        debug!("Access not granted");
        HttpResponse::Unauthorized().finish()
    }
}

fn points_response_aggregator(from: i64, to: i64, quest_id: i32) -> String {
    let mut output: String = String::from("[\n");

    debug!("Filter between {} and {}", from, to);

    let mut written = false;

    for point in Datapoint::find_all().unwrap() {
        // TODO make more efficient by getting from database pre-selected
        if point.time > from && point.time < to && (quest_id < 0 || point.quest_id == quest_id) {
            written = true;

            write!(
                output,
                "[{}, {}, {}, {}, {}],\n",
                point.ident, point.time, point.lat, point.lon, point.quest_id
            )
            .unwrap();
        }
    }

    if written {
        // remove the comma only if one point was written
        output.pop();
        output.pop();
    }

    write!(output, "\n]").unwrap();

    return output;
}

pub async fn request_photo_meta_route(
    query: web::Query<HashMap<String, String>>,
) -> impl Responder {
    debug!("Requested photos meta");
    let secret = query.get("secret").map_or("", String::as_str);
    let access: bool = check_secret(secret);

    let from = query
        .get("from")
        .and_then(|f| f.parse::<i64>().ok())
        .unwrap_or(1264979220000); // 1264979220000 = 01.01.2010
    let to = query
        .get("to")
        .and_then(|t| t.parse::<i64>().ok())
        .unwrap_or(2527283220000); // 2527283220000 = 01.01.2050

    if access {
        debug!("Access granted, sending data");

        HttpResponse::Ok()
            .content_type(ContentType::json())
            .body(photo_meta_response_aggregator(from, to, -1))
    } else {
        debug!("Access not granted");
        HttpResponse::Unauthorized().finish()
    }
}

fn photo_meta_response_aggregator(from: i64, to: i64, quest_id: i32) -> String {
    let mut output: String = String::from("[\n");

    debug!("Filter between {} and {}", from, to);

    let mut written = false;

    for photo in Photo::find_all_meta().unwrap() {
        // TODO make more efficient by getting from database pre-selected
        if photo.time > from && photo.time < to && (quest_id < 0 || photo.quest_id == quest_id) {
            written = true;

            write!(
                output,
                "[{}, {}, {}, {}, {}],\n",
                photo.id, photo.time, photo.lat, photo.lon, photo.quest_id
            )
            .unwrap();
        }
    }

    if written {
        // remove the comma only if one photo was written
        output.pop();
        output.pop();
    }

    write!(output, "\n]").unwrap();

    return output;
}

pub async fn request_view_control_struct_content(
    query: web::Query<HashMap<String, String>>,
) -> impl Responder {
    debug!("Requested View Control Struct Data");
    let slug = query.get("slug").map_or("", String::as_str);

    let vc_struct = match ViewControlStruct::find(String::from(slug)) {
        Ok(vc_struct) => vc_struct,
        Err(_) => {
            debug!("Something failed, could not find slug correspondence");
            return HttpResponse::Unauthorized().finish();
        }
    };

    let secret = query.get("secret").map_or("", String::as_str);
    let access: bool = vc_struct.password == String::from(secret);

    if access {
        debug!("Access granted, sending data");

        let mut written = false;
        let mut output: String = String::from("[\n");

        for group in vc_struct.quest_selections {
            written = true;

            write!(
                output,
                "{{\"quest_id\": {}, \"name\": \"{}\", \"points\": {}, \"photos\": {}}},\n",
                group.quest_id,
                group.name,
                points_response_aggregator(
                    group.start_timestamp,
                    group.end_timestamp,
                    group.quest_id
                ),
                photo_meta_response_aggregator(
                    group.start_timestamp,
                    group.end_timestamp,
                    group.quest_id
                )
            )
            .unwrap();
        }

        if written {
            // remove the comma only if one photo was written
            output.pop();
            output.pop();
        }
        write!(output, "\n]").unwrap();

        HttpResponse::Ok()
            .content_type(ContentType::json())
            .body(format!(
                "{{\"name\": \"{}\", \"groups\": {}}}",
                vc_struct.name, output
            ))
    } else {
        debug!("Access not granted");
        HttpResponse::Unauthorized().finish()
    }
}

pub async fn request_photo_route(query: web::Query<HashMap<String, String>>) -> impl Responder {
    debug!("Requested photo data");
    let secret = query.get("secret").map_or("", String::as_str);
    let mut access: bool = check_secret(secret);

    let id = query
        .get("id")
        .and_then(|t| t.parse::<i32>().ok())
        .unwrap_or(-1);

    let photo = match Photo::find(id) {
        Ok(photo) => photo,
        Err(_) => {
            debug!("Something failed, could not find corresponding photo");
            return HttpResponse::Unauthorized().finish();
        }
    };

    if !access {
        // Extra access because of knowing one password for a collection that contains the photo
        let control_structs = match ViewControlStruct::find_all() {
            Ok(control_structs) => control_structs,
            Err(_) => {
                debug!("Something failed, could not find control structs");
                return HttpResponse::Unauthorized().finish();
            }
        };

        if control_structs.into_iter().any(|vcs| {
            if vcs.password != String::from(secret) {
                return false;
            }

            return vcs.quest_selections.iter().any(|qs| {
                return qs.start_timestamp <= photo.time
                    && qs.end_timestamp >= photo.time
                    && photo.quest_id == qs.quest_id;
            });
        }) {
            access = true;
        }
    }

    if access {
        debug!("Access granted, sending data");

        HttpResponse::Ok().json(photo)
    } else {
        debug!("Access not granted");
        HttpResponse::Unauthorized().finish()
    }
}
