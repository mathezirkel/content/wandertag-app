// @generated automatically by Diesel CLI.

pub mod sql_types {
    #[derive(diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "quest_selection"))]
    pub struct QuestSelection;
}

diesel::table! {
    datapoints (id) {
        id -> Int4,
        lat -> Float4,
        lon -> Float4,
        time -> Int8,
        ident -> Int4,
        quest_id -> Int4,
    }
}

diesel::table! {
    photos (id) {
        id -> Int4,
        quest_id -> Int4,
        lat -> Float4,
        lon -> Float4,
        time -> Int8,
        image -> Text,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::QuestSelection;

    view_control_structs (id) {
        id -> Int4,
        slug -> Text,
        name -> Text,
        password -> Text,
        quest_selections -> Array<QuestSelection>,
    }
}

diesel::allow_tables_to_appear_in_same_query!(
    datapoints,
    photos,
    view_control_structs,
);
