
CREATE TABLE "photos"
(
    id SERIAL PRIMARY KEY,
    quest_id INT NOT NULL DEFAULT 0,
    lat REAL NOT NULL,
    lon REAL NOT NULL,
    time BIGINT NOT NULL,    
    image TEXT NOT NULL
);
