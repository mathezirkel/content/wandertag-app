
CREATE TYPE quest_selection AS (
    start_timestamp BIGINT,
    end_timestamp BIGINT,
    name TEXT,
    quest_id INT
);


CREATE TABLE "view_control_structs"
(
    id SERIAL PRIMARY KEY,
    slug TEXT NOT NULL,
    name TEXT NOT NULL,
    password TEXT NOT NULL, -- This in plaintext is fine for this application, as it is only our data, never user's pw
    quest_selections quest_selection[] NOT NULL DEFAULT array[]::quest_selection[],
    UNIQUE(slug)
);
