use crate::database::photos;
use crate::db;
use crate::error_handler::CustomError;
use diesel::prelude::*;
use diesel::Insertable;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, AsChangeset, Insertable, Queryable)]
#[diesel(table_name = photos)]
pub struct InsertPhoto {
    pub quest_id: i32,
    pub lat: f32,
    pub lon: f32,
    pub time: i64,
    pub image: String,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable)]
#[diesel(table_name = photos)]
pub struct PhotoMeta {
    pub id: i32,
    pub quest_id: i32,
    pub lat: f32,
    pub lon: f32,
    pub time: i64,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable)]
#[diesel(table_name = photos)]
pub struct Photo {
    pub id: i32,
    pub quest_id: i32,
    pub lat: f32,
    pub lon: f32,
    pub time: i64,
    pub image: String,
}
impl Photo {
    pub fn find_all_meta() -> Result<Vec<PhotoMeta>, CustomError> {
        Ok(photos::table
            .select((
                photos::id,
                photos::quest_id,
                photos::lat,
                photos::lon,
                photos::time,
            ))
            .load::<PhotoMeta>(&mut db::connection()?)?)
    }

    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(photos::table.load::<Photo>(&mut db::connection()?)?)
    }

    pub fn find(id: i32) -> Result<Self, CustomError> {
        Ok(photos::table
            .filter(photos::id.eq(id))
            .first(&mut db::connection()?)?)
    }

    pub fn create(photo: InsertPhoto) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(photos::table)
            .values(photo)
            .get_result(&mut db::connection()?)?)
    }
}
