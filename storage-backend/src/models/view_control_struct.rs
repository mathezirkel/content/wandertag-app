use crate::database::view_control_structs;
use crate::db;
use crate::error_handler::CustomError;
use diesel::{
    deserialize::{self, FromSql},
    pg::{Pg, PgValue},
    prelude::QueryableByName,
    prelude::*,
    serialize::{self, Output, ToSql, WriteTuple},
    sql_types::Record,
    Insertable,
};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq)]
pub struct QuestSelectionInput {
    start_timestamp: i64,
    end_timestamp: i64,
    name: String,
    quest_id: i32,
}

impl ToSql<crate::database::sql_types::QuestSelection, Pg> for QuestSelectionInput {
    fn to_sql<'b>(&'b self, out: &mut Output<'b, '_, Pg>) -> serialize::Result {
        WriteTuple::<(
            diesel::sql_types::BigInt,
            diesel::sql_types::BigInt,
            diesel::sql_types::Text,
            diesel::sql_types::Integer,
        )>::write_tuple(
            &(
                self.start_timestamp,
                self.end_timestamp,
                &self.name,
                self.quest_id,
            ),
            out,
        )
    }
}

#[derive(Debug, QueryableByName, Deserialize, Serialize, Clone, PartialEq)]
pub struct QuestSelection {
    #[diesel(sql_type = diesel::sql_types::BigInt)]
    pub start_timestamp: i64,
    #[diesel(sql_type = diesel::sql_types::BigInt)]
    pub end_timestamp: i64,
    #[diesel(sql_type = diesel::sql_types::Text)]
    pub name: String,
    #[diesel(sql_type = diesel::sql_types::Integer)]
    pub quest_id: i32,
}

impl FromSql<crate::database::sql_types::QuestSelection, Pg> for QuestSelection {
    fn from_sql(bytes: PgValue) -> deserialize::Result<Self> {
        let (start_timestamp, end_timestamp, name, quest_id) = FromSql::<
            Record<(
                diesel::sql_types::BigInt,
                diesel::sql_types::BigInt,
                diesel::sql_types::Text,
                diesel::sql_types::Integer,
            )>,
            Pg,
        >::from_sql(bytes)?;
        Ok(QuestSelection {
            start_timestamp,
            end_timestamp,
            name,
            quest_id,
        })
    }
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, Queryable)]
#[diesel(table_name = view_control_structs)]
pub struct InsertViewControlStruct {
    pub slug: String,
    pub name: String,
    pub password: String,
    pub quest_selections: Vec<QuestSelectionInput>,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable)]
#[diesel(table_name = view_control_structs)]
pub struct ViewControlStruct {
    pub id: i32,
    pub slug: String,
    pub name: String,
    pub password: String,
    pub quest_selections: Vec<QuestSelection>,
}

impl ViewControlStruct {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(view_control_structs::table.load::<Self>(&mut db::connection()?)?)
    }

    pub fn find(slug: String) -> Result<Self, CustomError> {
        Ok(view_control_structs::table
            .filter(view_control_structs::slug.eq(slug))
            .first(&mut db::connection()?)?)
    }

    pub fn create(vc_struct: InsertViewControlStruct) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(view_control_structs::table)
            .values(vc_struct)
            .get_result::<Self>(&mut db::connection()?)?)
    }

    pub fn update(id: i32, vc_struct: InsertViewControlStruct) -> Result<Self, CustomError> {
        Ok(diesel::update(view_control_structs::table)
            .filter(view_control_structs::id.eq(id))
            .set(vc_struct)
            .get_result(&mut db::connection()?)?)
    }

    pub fn delete(id: i32) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::delete(view_control_structs::table.filter(view_control_structs::id.eq(id)))
                .execute(&mut db::connection()?)?,
        )?)
    }
}
