#[macro_use]
extern crate log;

mod database;
mod db;
mod environment;
mod error_handler;
mod models;
mod points_photos;
mod view_management;

use actix_cors::Cors;
use actix_web::{
    http::header, http::header::ContentType, middleware, web, App, HttpResponse, HttpServer,
    Responder,
};
use dotenv::dotenv;
use points_photos::{
    request_photo_meta_route, request_photo_route, request_route,
    request_view_control_struct_content, upload_photo_route, upload_route,
};
use std::env;
use view_management::{
    create_control_structs_route, delete_control_structs_route, get_all_control_structs_route,
    update_control_structs_route,
};

async fn init_route() -> impl Responder {
    debug!("Init");
    HttpResponse::Ok()
        .content_type(ContentType::plaintext())
        .body("Datapoint Logging Server works")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    dotenv().ok();
    db::init();

    let server = HttpServer::new(move || {
        App::new()
            .app_data(web::PayloadConfig::new(7000000)) // 7mb payload limit
            .app_data(web::JsonConfig::default().limit(7000000)) // 7mb json limit
            .wrap(
                Cors::default()
                    .allow_any_origin()
                    .allowed_methods(vec!["POST", "GET"])
                    .allowed_headers(vec![header::AUTHORIZATION, header::ACCEPT])
                    .allowed_header(header::CONTENT_TYPE)
                    .supports_credentials()
                    .max_age(3600),
            )
            .wrap(middleware::Compress::default())
            .wrap(middleware::Logger::default())
            .service(web::resource("/").route(web::get().to(init_route)))
            .service(web::resource("/log-data").route(web::post().to(upload_route)))
            .service(web::resource("/upload-photo").route(web::post().to(upload_photo_route)))
            .service(web::resource("/request-data").route(web::get().to(request_route)))
            .service(
                web::resource("/view-control-structs")
                    .route(web::get().to(get_all_control_structs_route)),
            )
            .service(
                web::resource("/create-control-struct")
                    .route(web::post().to(create_control_structs_route)),
            )
            .service(
                web::resource("/update-control-struct")
                    .route(web::post().to(update_control_structs_route)),
            )
            .service(
                web::resource("/delete-control-struct")
                    .route(web::post().to(delete_control_structs_route)),
            )
            .service(
                web::resource("/get-view-data")
                    .route(web::get().to(request_view_control_struct_content)),
            )
            .service(
                web::resource("/request-photo-meta").route(web::get().to(request_photo_meta_route)),
            )
            .service(web::resource("/request-photo").route(web::get().to(request_photo_route)))
    });

    let server_host =
        env::var("WEBSERVER_HOST").expect("Webserver host ('WEBSERVER_HOST') not set in .env");
    let server_port =
        env::var("WEBSERVER_PORT").expect("Webserver port ('WEBSERVER_PORT') not set in .env");

    info!("Server started on http://{}:{}", server_host, server_port);

    server
        .bind(format!("{}:{}", server_host, server_port))
        .unwrap()
        .run()
        .await
}

// build change comment v1
