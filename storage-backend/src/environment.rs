use std::env;

pub fn upload_active() -> bool {
    return env::var("UPLOAD_ACTIVE").unwrap_or(String::from("false")) == String::from("true");
}

pub fn check_secret(secret: &str) -> bool {
    let comparison =
        env::var("GET_DATA_SECRET").expect("Value ('GET_DATA_SECRET') not set in .env");

    // TODO: use constant-time comparison function to avoid timing attacks
    return comparison == *secret;
}
